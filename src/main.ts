import { createApp } from 'vue'
import App from './App.vue'
import '@/style/default.css'

createApp(App).mount('#app')
