<template>
  <canvas ref="canvas" :width="width" :height="height"/>
</template>
<script lang="ts">
import {defineComponent, onMounted, onUnmounted, ref, watch} from "vue";
import type {PropType} from "vue";
import throttleOnAnimationFrame from "@/decorators/throttleOnAnimationFrame";

type Point = {
  x: number
  y: number
}
type MovingPoint = {
  vector: {
    x: number
    y: number
  },
  speed: number
} & Point
type MouseInformation = {
  coords: Point,
  pressed: number
}

const MILLISECONDS_IN_SECOND = 1000

export default defineComponent({
  name: "DotsCanvas",
  props: {
    width: {
      type: Number,
      default: 1000
    },
    height: {
      type: Number,
      default: 1000
    },
    minSpeed: {
      type: Number,
      default: 1
    },
    maxSpeed: {
      type: Number,
      default: 5
    },
    lineWidth: {
      type: Number,
      default: 2
    },
    maxPointsDistance: {
      type: Number,
      default: 500
    },
    fullLineWidthPointsDistance: {
      type: Number,
      default: 150
    },
    showMouseCursorLines: {
      type: Boolean,
      default: false
    },
    pointsNumber: {
      type: Number,
      default: 20
    },
    gravityPower: {
      type: Number,
      default: 1
    },
    pointSize: {
      type: Number,
      default: 2
    },
    pointColor: {
      type: String,
      default: "#ff0000"
    },
    lineColor: {
      type: String,
      default: "#000000"
    },
    backgroundColor: {
      type: String,
      default: "#ffffff"
    },
    gravityColor: {
      type: String as PropType<string | null>,
      default: null
    },
    splashesColor: {
      type: String as PropType<string | null>,
      default: null
    },
    splashSize: {
      type: Number,
      default: 20
    },
    maxSplashesFrames: {
      type: Number,
      default: 4
    }
  },
  setup(props) {
    const canvas = ref(null as any as HTMLCanvasElement)
    const points: MovingPoint[] = []
    const mouse: MouseInformation = {
      coords: {
        x: 0,
        y: 0
      },
      pressed: false
    }
    let lastRedraw = performance.now()

    const handlers = {
      onMouseDown() {
        canvas.value.removeEventListener('mousedown', handlers.onMouseDown)
        window.addEventListener('mouseup', handlers.onMouseUp)
        mouse.pressed = true
      },
      onMouseUp() {
        canvas.value.addEventListener('mousedown', handlers.onMouseDown)
        window.removeEventListener('mouseup', handlers.onMouseUp)
        mouse.pressed = false
      },
      onMouseMove: throttleOnAnimationFrame(function (e: MouseEvent) {
        const {width, height} = canvas.value.getBoundingClientRect()
        mouse.coords = {
          x: props.width * e.offsetX / width,
          y: props.height * e.offsetY / height
        }
      })
    }
    const distanceCalculation = {
      getDistanceBetweenPoints(p1: Point, p2: Point) {
        return Math.sqrt((p2.x - p1.x)**2 + (p2.y - p1.y)**2)
      },
      getLineWidthDependingOnDistance(distance: number) {
        if (distance > props.maxPointsDistance) {
          return 0
        }
        if (distance <= props.fullLineWidthPointsDistance) {
          return props.lineWidth
        }

        const delta = distance - props.fullLineWidthPointsDistance
        const maximalDelta = props.maxPointsDistance - props.fullLineWidthPointsDistance

        return props.lineWidth * (1 - delta / maximalDelta)
      }
    }
    const pointsGeneration = {
      generatePoint(width: number, height: number) {
        return {
          x: Math.random() * width,
          y: Math.random() * height,
          vector: {
            x: Math.random() * 2 - 1,
            y: Math.random() * 2 - 1
          },
          speed: Math.random() * (props.maxSpeed - props.minSpeed) + props.minSpeed
        }
      },
      generatePoints(amount: number) {
        const {width, height} = canvas.value
        for (let i = 0; i < amount; i++) {
          points.push(pointsGeneration.generatePoint(width, height))
        }
      }
    }
    const canvasDrawing = {
      splashesFrames: [] as Point[][],
      addSplash(x: number, y: number) {
        if (canvasDrawing.splashesFrames.length === 0) {
          canvasDrawing.splashesFrames.push([])
        }
        canvasDrawing.splashesFrames[canvasDrawing.splashesFrames.length - 1].push({
          x,
          y
        })
      },
      drawBackground(ctx: CanvasRenderingContext2D, el: HTMLCanvasElement) {
        ctx.fillStyle = props.backgroundColor
        ctx.fillRect(0,0, el.width, el.height)
      },
      drawSplashes(ctx: CanvasRenderingContext2D) {
        if (!props.splashesColor || !canvasDrawing.splashesFrames.length) {
          canvasDrawing.splashesFrames.length = 0
          return
        }

        ctx.fillStyle = props.splashesColor
        ctx.strokeStyle = props.lineColor

        for (const frame of canvasDrawing.splashesFrames) {
          for (const splash of frame) {
            ctx.beginPath()
            ctx.arc(
                splash.x,
                splash.y,
                props.splashSize,
                0,
                2 * Math.PI
            )
            ctx.fill()
            ctx.stroke()
            ctx.closePath()
          }
        }

        canvasDrawing.splashesFrames.push([])

        if (canvasDrawing.splashesFrames.length > props.maxSplashesFrames) {
          canvasDrawing.splashesFrames.splice(0, canvasDrawing.splashesFrames.length - props.maxSplashesFrames)
        }
      },
      drawGravityGradient(ctx: CanvasRenderingContext2D, el: HTMLCanvasElement) {
        if (!props.gravityColor || !mouse.pressed) {
          return
        }

        const gradient = ctx.createRadialGradient(
            mouse.coords.x,
            mouse.coords.y,
            0,
            mouse.coords.x,
            mouse.coords.y,
            Math.max(el.width, el.height));
        gradient.addColorStop(0, props.gravityColor);
        gradient.addColorStop(0.1, props.gravityColor);
        gradient.addColorStop(1, props.backgroundColor);
        ctx.beginPath();
        ctx.arc(
            mouse.coords.x,
            mouse.coords.y,
            Math.max(el.width, el.height),
            0,
            2 * Math.PI
        );
        ctx.fillStyle = gradient;
        ctx.fill();
        ctx.closePath()
      },
      drawLine(ctx: CanvasRenderingContext2D, p1: Point, p2: Point) {
        const distanceBetweenPoints = distanceCalculation.getDistanceBetweenPoints(p1, p2)
        const lineWidth = distanceCalculation.getLineWidthDependingOnDistance(distanceBetweenPoints)

        if (lineWidth === 0) {
          return
        }

        ctx.lineWidth = lineWidth
        ctx.beginPath()
        ctx.moveTo(p1.x, p1.y)
        ctx.lineTo(p2.x, p2.y)
        ctx.stroke()
      },
      drawLines(ctx: CanvasRenderingContext2D) {
        ctx.strokeStyle = props.lineColor

        for (let i = 0; i < points.length; i++) {
          const p1 = points[i]

          for (let j = i + 1; j < points.length; j++) {
            const p2 = points[j];
            canvasDrawing.drawLine(ctx, p1, p2)
          }

          if (props.showMouseCursorLines) {
            canvasDrawing.drawLine(ctx, p1, mouse.coords)
          }
        }
      },
      drawPoints(ctx: CanvasRenderingContext2D) {
        const pointSize = props.pointSize
        if (pointSize === 0) {
          return
        }

        ctx.fillStyle = props.pointColor

        for (const point of points) {
          ctx.beginPath()
          ctx.arc(
              point.x,
              point.y,
              props.pointSize,
              0,
              2 * Math.PI
          )
          ctx.fill()
          ctx.closePath()
        }
      },
      redrawCanvas() {
        const el = canvas.value
        const ctx = el.getContext("2d")

        if (!ctx || !el) {
          throw new Error("No canvas!")
        }

        canvasDrawing.drawBackground(ctx, el)
        canvasDrawing.drawGravityGradient(ctx, el)
        canvasDrawing.drawSplashes(ctx)
        canvasDrawing.drawLines(ctx)
        canvasDrawing.drawPoints(ctx)
      }
    }
    const pointsMovement = {
      gravityTick(point: MovingPoint) {
        if (!mouse.pressed) {
          return
        }

        const distance = distanceCalculation.getDistanceBetweenPoints(point, mouse.coords);
        const gravityVector = {
          x: (mouse.coords.x - point.x) / distance,
          y: (mouse.coords.y - point.y) / distance,
        };
        const intensity = props.gravityPower / distance;

        const existingVectorRemainingPart = Math.max(1 - intensity, 0)
        const addedVectorPart = Math.min(intensity, 1)
        point.vector.x = point.vector.x * existingVectorRemainingPart + gravityVector.x * addedVectorPart;
        point.vector.y = point.vector.y * existingVectorRemainingPart + gravityVector.y * addedVectorPart;
        point.speed = (() => {
          const similarityX = 1 - Math.abs(point.vector.x - gravityVector.x)
          const similarityY = 1 - Math.abs(point.vector.y - gravityVector.y)

          return point.speed + (similarityY + similarityX) * (point.speed * intensity)
        })()
      },
      inertialMovementTick(point: MovingPoint, timeDelta: number) {
        const secondsPassed = timeDelta / MILLISECONDS_IN_SECOND
        const {width, height} = canvas.value

        point.x = point.x + point.speed * secondsPassed * point.vector.x
        point.y = point.y + point.speed * secondsPassed * point.vector.y

        if (!(point.x < 0 || point.y < 0 || point.x > width || point.y > height)) {
          return
        }

        if (point.x < 0) {
          point.vector.x = Math.random()
          point.x = 0
        } else if (point.x > width) {
          point.vector.x = -Math.random()
          point.x = width
        }

        if (point.y < 0) {
          point.y = 0
          point.vector.y = Math.random()
        } else if (point.y > height) {
          point.y = height
          point.vector.y = -Math.random()
        }

        canvasDrawing.addSplash(point.x, point.y)

        point.speed = Math.random() * (props.maxSpeed - props.minSpeed) + props.minSpeed
      },
      tick() {
        const timeDelta = performance.now() - lastRedraw
        lastRedraw = performance.now()

        // If frames rendering was stopped for long time (for example if browser tab was not active),
        // then skip one frame to avoid strange points behavior
        if (timeDelta > 2000) {
          return
        }

        for (const point of points) {
          pointsMovement.gravityTick(point)
          pointsMovement.inertialMovementTick(point, timeDelta)
        }
      }
    }

    let animationFrameRequestKey = null as any as number
    const stopTicking = () => cancelAnimationFrame(animationFrameRequestKey)
    const tick = () => {
      canvasDrawing.redrawCanvas()
      pointsMovement.tick()
      animationFrameRequestKey = requestAnimationFrame(tick)
    }

    onMounted(() => {
      canvas.value.addEventListener('mousedown', handlers.onMouseDown)
      canvas.value.addEventListener('mousemove', handlers.onMouseMove)

      pointsGeneration.generatePoints(props.pointsNumber)
      tick()
    })
    onUnmounted(stopTicking)
    watch(() => props.pointsNumber, (value, oldValue) => {
        if (value > oldValue) {
          pointsGeneration.generatePoints(value - oldValue)
        } else {
          points.length = value
        }
      })

    return {
      canvas
    }
  }
})
</script>
<style scoped>

</style>