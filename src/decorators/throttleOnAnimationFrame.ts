export default function throttleOnAnimationFrame<T extends (...args: any[]) => any>(func: T): T {
    let animationFrameRequestKey = null

    return (function throttledFunc(...args) {
        cancelAnimationFrame(animationFrameRequestKey)
        animationFrameRequestKey = requestAnimationFrame(() => {
            func.apply(this, args)
        })
    }) as T
}